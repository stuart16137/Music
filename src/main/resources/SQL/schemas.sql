
CREATE TABLE IF NOT EXISTS playlist (
  playlistName VARCHAR(75),
  songName VARCHAR(75),
  artist VARCHAR(75),
  album VARCHAR(75),
  runTime VARCHAR(10),

  PRIMARY KEY (playlistName, songName)
);
