import controller.MusicLibraryController;
import javafx.application.Application;
import javafx.stage.Stage;
import org.apache.log4j.BasicConfigurator;
import view.Views;

public class MusicPlayerApp extends Application{
    public static void main(String[] args){
        try {
            BasicConfigurator.configure();
            launch(args);
        }catch (Exception e){
            System.out.print(e + ": " + e.getMessage());
        }
    }

    public void start(Stage primaryStage) throws Exception {
        Views appView = new Views(primaryStage);
        MusicLibraryController MLC = new MusicLibraryController();
        MLC.setAppViews(appView);
        appView.homeView();
    }
}
