package model;

import java.util.ArrayList;

public class Playlist {
    private String name;
    private ArrayList<Song> songs;

    public Playlist(String name) throws IllegalArgumentException{
        try{
            this.setName(name);
        } catch (IllegalArgumentException ie) {
            throw ie;
        }
        songs = new ArrayList();
    }

    public void setName(String name) throws IllegalArgumentException{
        if(!validName(name))
            throw new IllegalArgumentException("Invalid Playlist Name");
        this.name = name;
    }

    public void addSong(Song song) throws IllegalArgumentException{
        if(song == null)
            throw new IllegalArgumentException("Invalid song");
        songs.add(song);
    }

    public void removeSong(Song song) throws IllegalArgumentException{
        if(song == null || !songs.contains(song))
            throw new IllegalArgumentException("Invalid song");
        songs.remove(song);
    }

    public String getName() {
        return name;
    }

    public ArrayList<Song> getSongs() {
        return songs;
    }

    public boolean validName(String name){
        try{
            if(name.length() < 1)
                return false;
            return true;
        } catch(Exception e){
            return false;
        }
    }
}
