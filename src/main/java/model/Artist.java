package model;

import java.util.ArrayList;

public class Artist {
    private String name;
    private String description;
    private ArrayList<Album> discography;


    public Artist(String name, String  description, ArrayList<Album> discography) throws IllegalArgumentException{
        try{
            this.setName(name);
            this.setDescription(description);
            this.setDiscography(discography);
        } catch(IllegalArgumentException e){
            throw e;
        }
    }

    public Artist(String name) throws IllegalArgumentException{
        if(!isValidNameOrDescription(name))
            throw new IllegalArgumentException("Invalid name");
        this.name = name;
        this.description = "*This artist does not yet have a description*";
        this.discography = new ArrayList<Album>();
    }

    public void setName(String name) throws IllegalArgumentException{
        if(!isValidNameOrDescription(name))
            throw new IllegalArgumentException("Invalid name");
        this.name = name;
    }

    public void setDescription(String description) throws  IllegalArgumentException{
        if(!isValidNameOrDescription(description))
            throw new IllegalArgumentException("Invalid description");
        this.description = description;
    }

    public void setDiscography(ArrayList<Album> discography) throws IllegalArgumentException{
        if(!isValidDiscography(discography))
            throw new IllegalArgumentException("Invalid discography");
        this.discography = discography;
    }

    public String getName(){
        return name;
    }

    public String getDescription() {
        return description;
    }

    public ArrayList<Album> getDiscography() {
        return discography;
    }

    private boolean isValidNameOrDescription(String test){
        try{
            if(test.length() < 1){
                return false;
            }
        } catch(Exception e){
            return false;
        }
        return true;
    }

    private boolean isValidDiscography(ArrayList<Album> discography){
        try{
            discography.get(0);
            return true;
        } catch(Exception e){
            return false;
        }
    }
}
