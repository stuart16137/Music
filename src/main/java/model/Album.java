package model;

import java.util.ArrayList;
import java.util.Date;
import  java.text.SimpleDateFormat;

public class Album {
    private String title;
    private Date releaseDate;
    private Artist artist;
    private ArrayList<Song> songs;

    public Album(String title, String releaseDate, Artist artist, ArrayList<Song> songs) throws IllegalArgumentException{
        try {
            this.setTitle(title);
            this.setArtist(artist);
            this.setReleaseDate(releaseDate);
            this.setSongs(songs);
        } catch (IllegalArgumentException e){
            throw e;
        }
    }

    public Album(String title) throws IllegalArgumentException{
        if(!validTitle(title)){
            throw new IllegalArgumentException("Invalid Title");
        }
        this.title = title;
        this.artist = new Artist("");
        this.releaseDate = null;
        this.songs = new ArrayList<>();
    }

    public void setTitle(String title) throws IllegalArgumentException{
        if(!validTitle(title))
            throw new IllegalArgumentException("Invalid title.");
        this.title = title;
    }

    public void setReleaseDate(String releaseDate) throws IllegalArgumentException{
        try {
            this.releaseDate = new SimpleDateFormat("dd/MM/yyyy").parse(releaseDate);
        } catch(Exception e){
            throw new IllegalArgumentException(e.getMessage());
        }
    }

    public void setArtist(Artist artist) throws IllegalArgumentException{
        if(!validArtist(artist))
            throw new IllegalArgumentException();
        this.artist = artist;
    }

    public void setSongs(ArrayList<Song> songs) throws IllegalArgumentException{
        if(!validSongs(songs))
            throw new IllegalArgumentException("Invalid list of songs.");
        this.songs = songs;
    }

    public String getTitle(){
        return title;
    }

    public String getReleaseDate(){
        return releaseDate.toString();
    }

    public Artist getArtist(){
        return artist;
    }

    public ArrayList<Song> getSongs() {
        return songs;
    }

    private boolean validTitle(String test) {
        try{
            if(test.length() < 1)
                return false;
            return true;
        } catch (Exception e){
            return false;
        }
    }

    private boolean validSongs(ArrayList<Song> test){
        try{
            test.isEmpty();
            return true;
        } catch(Exception e) {
            return false;
        }
    }

    private boolean validArtist(Artist artist){

        try{
            if(artist.getName().length() < 1)
                return false;
            return true;
        } catch (Exception e){
            return false;
        }
    }
}
