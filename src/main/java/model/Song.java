package model;

public class Song {
    private String title;
    private Album album;
    private Artist artist;
    private double runTime;

    public Song(String title, Artist artist, Album album, double runTime) throws IllegalArgumentException{
        try{
            this.setTitle(title);
            this.setArtist(artist);
            this.setAlbum(album);
            this.setRunTime(runTime);
        } catch(IllegalArgumentException e){
            throw e;
        }
    }

    public void setTitle(String title) throws IllegalArgumentException{
        this.title = title;
    }

    public void setArtist(Artist artist) throws IllegalArgumentException{
        try{
            artist.getName().length();
        }catch(Exception e){
            throw new IllegalArgumentException("INVALID ARTIST");
        }
        this.artist = artist;
    }

    public void setAlbum(Album album) throws IllegalArgumentException{
        try{
            album.getTitle().length();
        }catch(Exception e){
            throw new IllegalArgumentException("INVALID ALBUM");
        }

        this.album = album;
    }

    public void setRunTime(double runTime) throws IllegalArgumentException{
        this.runTime = runTime;
    }

    public String getTitle(){
        return title;
    }

    public Album getAlbum(){
        return album;
    }

    public Artist getArtist(){
        return artist;
    }

    public double getRunTime(){
        return runTime;
    }
}
