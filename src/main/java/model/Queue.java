package model;



import java.util.LinkedList;

public class Queue {

    private LinkedList<Song> queue;

    public Queue(){
        queue = new LinkedList<Song>();
    }

    public void add(Song song) throws IllegalArgumentException{
        if(song == null)
            throw new IllegalArgumentException("Invalid Song");
        queue.addLast(song);
    }

    public boolean contains(Song song) {
        return queue.contains(song);
    }

    public void remove(Song song) throws IllegalArgumentException{
        if(!queue.contains(song))
            throw new IllegalArgumentException("Song not in queue");
        queue.remove(song);
    }

    public Song get(int i) throws IndexOutOfBoundsException {
        try{
            return queue.get(i);
        } catch(Exception e){
            throw new IndexOutOfBoundsException();
        }
    }

    public Song removeHead() throws IndexOutOfBoundsException{
        if(queue.isEmpty())
            throw new IndexOutOfBoundsException("Empty Queue");
        return queue.removeFirst();
    }

    public void move(Song song, int i) throws IllegalArgumentException{
        try{
            queue.remove(song);
            queue.add(i, song);
        } catch(Exception e){
            throw new IllegalArgumentException(e.getMessage());
        }
    }
}
