package controller;


import dao.JdbcPlaylistDao;
import dao.PlaylistDao;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import model.Playlist;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import view.Views;

import java.util.ArrayList;
import java.util.List;

public class MusicLibraryController {

    private static Views appViews;
    public RadioButton playlistList;
    public RadioButton songList;
    public RadioButton artistList;
    public RadioButton albumList;
    public Button createPlaylistButton;

    public TableView<String> libraryContent;
    public TableColumn<String, String> entityName;

    private final PlaylistDao playlistDao;

    private static final Logger LOGGER = LoggerFactory.getLogger(JdbcPlaylistDao.class);

    public MusicLibraryController(){
        playlistDao = new JdbcPlaylistDao();
        setUpButtons();
        setUpTable();
    }

    private void setUpButtons(){
        final ToggleGroup group = new ToggleGroup();

        playlistList = new RadioButton();
        playlistList.setToggleGroup(group);

        songList = new RadioButton();
        songList.setToggleGroup(group);
        songList.setSelected(true);

        artistList = new RadioButton();
        artistList.setToggleGroup(group);

        albumList = new RadioButton();
        albumList.setToggleGroup(group);
    }

    private void setUpTable(){
        try {
            libraryContent = new TableView();
            entityName = new TableColumn();
            entityName.setCellValueFactory(
                    new PropertyValueFactory<>("entityName"));
            libraryContent.setEditable(true);
            libraryContent.getColumns().add(entityName);
        } catch (Exception e){
            LOGGER.error("Error Setting Up Table Column: " + e + "-" +e.getMessage());
        }
    }

    public void setAppViews(Views ui){
        appViews = ui;
    }

    @FXML
    void playlistSelected() {
        try {
            List<String> playlistNames = playlistDao.getPlaylists();
            populateTable(playlistNames);
        } catch(Exception e) {
            LOGGER.error("Error Fetching Playlists: " + e + "-" +e.getMessage());
        }
    }

    @FXML
    void artistSelected(){}

    @FXML
    void albumSelected(){}

    @FXML
    void songSelected(){}

    @FXML
    void createPlayList() {
        try {
            PlaylistController pc = new PlaylistController();
            pc.setViews(appViews);
            appViews.playlistView();
        } catch (Exception e) {
            LOGGER.error("Error Creating Playlist: " + e + "-" + e.getMessage());
        }
    }

    private void populateTable(List<String> entities){
        try {
            ObservableList<String> entityNames = FXCollections.observableArrayList();
            for(String entity: entities) {
                entityNames.add(entity);
            }
            libraryContent.setItems(entityNames);
        } catch (Exception e){
            LOGGER.error("Error Listing Playlists: " + e + "-" +e.getMessage());
        }
    }

    private TableColumn<String, String> createColumn(String columnName){
        TableColumn<String, String> column = new TableColumn<>(columnName);
        column.setCellValueFactory(new PropertyValueFactory<>(columnName));
        column.setCellFactory(TextFieldTableCell.<String>forTableColumn());
        return column;
    }
}
