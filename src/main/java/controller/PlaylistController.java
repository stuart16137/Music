package controller;

import dao.PlaylistDao;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import model.Playlist;
import view.Views;

public class PlaylistController{

    public TextField playlistName;
    public Button createButton;
    public Button cancel;
    private static Views appViews;

    public void setViews(Views view){
        appViews = view;
    }

    @FXML
    void cancel(){
        closeView();
    }

    @FXML
    void createPlaylist(){
        try {
            String name = playlistName.getText();
            Playlist p = new Playlist(name);
        } catch(Exception e){
            System.out.println(e + ": " + e.getMessage());
        }
    }

    private void closeView(){
        try {
            appViews.homeView();
        } catch (Exception e){
            System.out.println(e + ": " + e.getMessage());
        }
    }
}
