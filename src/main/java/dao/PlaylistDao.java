package dao;

import model.Playlist;
import model.Song;

import java.util.List;

public interface PlaylistDao {

    List<String> getPlaylists();

    Playlist getPlaylist(String playlistName);

    void createPlaylist(Playlist playlist);

    void addToPlaylist(Playlist playlist, Song song);

    void removeFromPlaylist(Playlist playlist, Song song);

    void renamePlaylist(Playlist playlist, String name);

    void deletePlaylist(Playlist playlist);
}
