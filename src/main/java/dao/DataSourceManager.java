package dao;

import dao.exception.DataSourceInstantiationException;
import dao.exception.DataSourceScriptException;
import org.hsqldb.jdbc.JDBCDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public abstract class DataSourceManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(DataSourceManager.class);

    private static DataSource dataSource;

    private DataSourceManager() {

    }

    public static synchronized DataSource getDataSource() {
        if (null == dataSource) {
            String connectionString = getConnectionString();

            LOGGER.info("Creating database with connection string {}", connectionString);

            JDBCDataSource dataSource = new JDBCDataSource();
            dataSource.setUrl(connectionString);
            dataSource.setUser("SA");
            dataSource.setPassword("");

            try {
                executeScripts(dataSource, "/SQL/schemas.sql", "/SQL/data.sql");
            } catch(Exception e) {
                LOGGER.info("Error initiating database");
            }

            DataSourceManager.dataSource = dataSource;
        }

        return dataSource;
    }

    private static String getConnectionString() {
        if (!"prod".equals(System.getProperty("mode"))) {
            LOGGER.info("Not in prod mode. Will use in memory database");

            return "jdbc:hsqldb:mem:persondb";
        }

        Path peopleDBDirectoryPath = Paths.get(System.getProperty("user.home")).resolve("PeopleDB");

        try {
            Files.createDirectories(peopleDBDirectoryPath);
        } catch (IOException e) {
            throw new DataSourceInstantiationException("Failed to create directory path " + peopleDBDirectoryPath, e);
        }

        Path databasePath = peopleDBDirectoryPath.resolve("database");

        LOGGER.info("Using a file to store database at {}", databasePath);

        return "jdbc:hsqldb:file:" + databasePath.toAbsolutePath();

    }

    public static void executeScripts(DataSource dataSource, String ...fileLocations) {
        for (String fileLocation : fileLocations) {
            LOGGER.info("Executing {}", fileLocation);

            try (Connection connection = dataSource.getConnection()) {
                for (String statement : getStatements(fileLocation)) {
                    try (PreparedStatement preparedStatement = connection.prepareStatement(statement.trim())) {
                        preparedStatement.executeUpdate();

                        connection.commit();
                    }
                }
            } catch (SQLException e) {
                throw new DataSourceScriptException("Failure when creating HSQLDB DataSource", e);
            }
        }

    }

    private static List<String> getStatements(String location) {
        StringBuilder stringBuilder = new StringBuilder();

        try (InputStream inputStream = DataSourceManager.class.getResourceAsStream(location)) {
            if (null != inputStream) {
                try (LineNumberReader lineNumberReader = new LineNumberReader(new InputStreamReader(inputStream))) {
                    String line = lineNumberReader.readLine();

                    while (null != line) {
                        if (!line.trim().isEmpty()) {
                            stringBuilder.append(line);

                            if (!line.endsWith(";")) {
                                stringBuilder.append(" ");
                            }
                        }

                        line = lineNumberReader.readLine();
                    }
                } catch (IOException e) {
                    throw new DataSourceInstantiationException("Failed to read " + location, e);
                }
            } else {
                throw new DataSourceScriptException("Failed to get InputStream for location " + location);
            }
        } catch (IOException e) {
            throw new DataSourceScriptException("Failed to get " + location + " file", e);
        }

        String[] statements = stringBuilder.toString().split(";");

        return Stream.of(statements)
                .filter(s -> !s.isEmpty())
                .collect(Collectors.toList());
    }

}
