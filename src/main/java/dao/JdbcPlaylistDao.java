package dao;

import model.Album;
import model.Artist;
import model.Playlist;
import model.Song;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class JdbcPlaylistDao implements PlaylistDao{


    private static final Logger LOGGER = LoggerFactory.getLogger(JdbcPlaylistDao.class);

    private final DataSource dataSource;

    public JdbcPlaylistDao(DataSource dataSource) {

        if(dataSource == null){
            throw new IllegalArgumentException("Data Source Null");
        }

        this.dataSource = dataSource;
    }

    public JdbcPlaylistDao() { this.dataSource = DataSourceManager.getDataSource();}

    public List<String> getPlaylists() {
        List<String> playlists = new ArrayList<>();

        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement("SELECT playlistName FROM playlist ORDER BY playlistName")) {

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                if(!playlists.contains(resultSet.getString("playlistName")))
                    playlists.add(resultSet.getString("playlistName"));
            }
        } catch (SQLException e) {
            LOGGER.error("Failed to get playlists from database", e);
        }

        return playlists;
    }

    public Playlist getPlaylist(String playlistName) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement("SELECT title, artist, album, runTime FROM playlist where playlistName = ? ORDER BY title")) {
            preparedStatement.setString(1, playlistName);

            Playlist playlist = new Playlist(playlistName);

            ResultSet resultSet = preparedStatement.executeQuery();
            while(resultSet.next()){
                playlist.addSong(new Song(
                        resultSet.getString("title"),
                        new Artist(resultSet.getString("artist")),
                        new Album(resultSet.getString("album")),
                        resultSet.getDouble("runTime")
                ));
            }


            return playlist;
        } catch (SQLException e) {
            LOGGER.error("Failed to get playlist with name {}", playlistName, e);
        }

        return null;    }

    public void createPlaylist(Playlist playlist) {
        LOGGER.info("Adding playlist to database: {}", playlist);

        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement( "INSERT INTO playlist (playListName) VALUES (?, ?, ?, ?, ?)")) {
            int updateCount = 0;
            for(Song song: playlist.getSongs()){
                preparedStatement.setString(1, playlist.getName());
                preparedStatement.setString(2, song.getTitle());
                preparedStatement.setString(3, song.getArtist().getName());
                preparedStatement.setString(4, song.getAlbum().getTitle());
                preparedStatement.setString(5, Double.toString(song.getRunTime()));
                updateCount = preparedStatement.executeUpdate();
            }

            LOGGER.info("{} rows added when adding {}", updateCount, playlist);
        } catch (SQLException e) {
            LOGGER.error("Failed to add {} to database", playlist, e);
        }

    }

    public void addToPlaylist(Playlist playlist, Song song) {
        LOGGER.info("Adding {} to {}", song, playlist);

        try (Connection connection = dataSource.getConnection();

             PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO playlist (playlistName, title, artist, album, runTime) VALUES (?,?,?,?,?)")) {

            preparedStatement.setString(1, playlist.getName());
            preparedStatement.setString(2, song.getTitle());
            preparedStatement.setString(3, song.getArtist().getName());
            preparedStatement.setString(4, song.getAlbum().getTitle());
            preparedStatement.setString(5, Double.toString(song.getRunTime()));


            int updateCount = preparedStatement.executeUpdate();

            LOGGER.info("{} rows added when adding {}", updateCount, playlist);
        } catch (SQLException except) {
            LOGGER.error("Failed to add {} to database", playlist, song);
        }
    }

    public void removeFromPlaylist(Playlist playlist, Song song) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM playlist WHERE playlistName = ? AND title = ?")) {
            preparedStatement.setString(1, playlist.getName());
            preparedStatement.setString(2, song.getTitle());

            int updateCount = preparedStatement.executeUpdate();

            LOGGER.info("Deleted {} rows from playlists table", updateCount);
        } catch (SQLException e) {
            LOGGER.error("Failure when deleting playlist with name {} from the database", playlist.getName(), e);
        }

    }

    public void renamePlaylist(Playlist playlist, String name) {
        String oldName = playlist.getName();
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement("UPDATE playlist SET playlistName = ? WHERE playlistName = ?")) {
            preparedStatement.setString(1, name);
            preparedStatement.setString(2, playlist.getName());

            LOGGER.info("Renamed playlist {} to {}", oldName, name);
        } catch (SQLException e) {
            LOGGER.error("Failure when renaming playlist {}", playlist.getName(), e);
        }
    }

    public void deletePlaylist(Playlist playlist) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM playlist WHERE playlistName = ?")) {
            preparedStatement.setString(1, playlist.getName());

            int updateCount = preparedStatement.executeUpdate();

            LOGGER.info("Deleted {} rows from playlists table", updateCount);
        } catch (SQLException e) {
            LOGGER.error("Failure when deleting playlist with name {} from the database", playlist.getName(), e);
        }
    }
}
