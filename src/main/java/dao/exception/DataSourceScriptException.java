package dao.exception;

public class DataSourceScriptException extends RuntimeException {

    public DataSourceScriptException(String message) {
        super(message);
    }

    public DataSourceScriptException(String message, Throwable cause) {
        super(message, cause);
    }
}
