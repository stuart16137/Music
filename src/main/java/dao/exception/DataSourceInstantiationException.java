package dao.exception;

public class DataSourceInstantiationException extends RuntimeException {

    public DataSourceInstantiationException(String message) {
        super(message);
    }

    public DataSourceInstantiationException(String message, Throwable cause) {
        super(message, cause);
    }
}
