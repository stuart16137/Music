package view;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.net.URL;

public class Views {

    private Stage primaryStage;
    private Parent homePage;
    private Scene home;
    private Parent playlistPage;



    public Views(Stage primaryStage)throws Exception{
        this.primaryStage = primaryStage;
        homePage = FXMLLoader.load(getClass().getResource("/FXML/MusicLibrary.fxml"));
        home = new Scene(homePage, 500, 500);
    }

    public void homeView() throws Exception{
        primaryStage.setTitle("Music Library");
        primaryStage.setScene(home);
        primaryStage.show();
    }

    public void playlistView()throws Exception{
        playlistPage = FXMLLoader.load(getClass().getResource("/FXML/CreatePlaylistView.fxml"));
        primaryStage.setTitle("Create Playlist");
        primaryStage.setScene(new Scene(playlistPage, 500, 500));
        primaryStage.show();
    }
}
