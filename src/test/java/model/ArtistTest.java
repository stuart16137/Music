package model;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

public class ArtistTest {

    private Artist testArtist;
    private Album mockedAlbum;
    private ArrayList<Album> mockedDiscography;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp() throws Exception {
        testArtist = new Artist("Test Artist");
        mockedDiscography = mock(ArrayList.class);
        mockedAlbum = mock(Album.class);
        when(mockedDiscography.get(0)).thenReturn(mockedAlbum);
        when(mockedAlbum.getTitle()).thenReturn("mock album");
    }

    @Test
    public void testCreateEmptyArtist(){
        testArtist = new Artist("Test");
        assertNotNull(testArtist);
    }
    @Test
    public void testInvalidEmptyArtist(){
        thrown.expect(IllegalArgumentException.class);
        testArtist = new Artist(null);
    }

    @Test
    public void testCreateNonEmptyArtist(){
        testArtist = new Artist("Test", "Description", mockedDiscography);
        assertNotNull(testArtist);
        assertNotNull(testArtist.getName());
        assertNotNull(testArtist.getDescription());
        assertTrue(testArtist.getDiscography().get(0).getTitle().equals("mock album"));
    }

    @Test
    public void setValidName() {
        testArtist.setName("new name");
        assertTrue(testArtist.getName().equals("new name"));
    }
    @Test
    public void setInvalidName() {
        thrown.expect(IllegalArgumentException.class);
        testArtist.setName("");
        thrown.expect(IllegalArgumentException.class);
        testArtist.setName(null);
    }

    @Test
    public void setValidDescription() {
        testArtist.setDescription("new description");
        assertTrue(testArtist.getDescription().equals("new description"));
    }

    @Test
    public void setEmptyDescription() {
        thrown.expect(IllegalArgumentException.class);
        testArtist.setDescription("");
        thrown.expect(IllegalArgumentException.class);
        testArtist.setDescription(null);
    }

    @Test
    public void setValidDiscography() {
        Album newAlbum = mock(Album.class);
        ArrayList<Album> newDiscography = mock(ArrayList.class);
        when(newDiscography.get(0)).thenReturn(newAlbum);
        when(newAlbum.getTitle()).thenReturn("new album");

        testArtist.setDiscography(newDiscography);
        assertTrue(testArtist.getDiscography().get(0).getTitle().equals("new album"));
    }

    @Test
    public void setInvalidDiscography() {
        thrown.expect(IllegalArgumentException.class);
        testArtist.setDiscography(null);
    }
}