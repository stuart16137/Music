package model;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.*;

public class PlaylistTest
{

    private Playlist testPlaylist;
    private Song mockSong;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp(){
        testPlaylist = new Playlist("test playlist");
    }

    @Test
    public void testCreateValidEmptyPlaylist(){
        testPlaylist = new Playlist("test playlist");
        assertNotNull(testPlaylist);
        assertNotNull(testPlaylist.getSongs());
        assertTrue(testPlaylist.getSongs().isEmpty());
    }

    @Test
    public void testCreateInvalidEmptyPlaylist(){
        thrown.expect(IllegalArgumentException.class);
        testPlaylist = new Playlist("");
        thrown.expect(IllegalArgumentException.class);
        testPlaylist = new Playlist(null);
    }

    @Test
    public void testSetValidTitle(){
        testPlaylist.setName("new title");
        assertTrue(testPlaylist.getName().equals("new title"));
    }

    @Test
    public void testSetInvalidTitle(){
        thrown.expect(IllegalArgumentException.class);
        testPlaylist.setName(null);
        thrown.expect(IllegalArgumentException.class);
        testPlaylist.setName("");
    }

    @Test
    public void testAddValidSong(){
        mockSong = mock(Song.class);
        when(mockSong.getTitle()).thenReturn("mock song");
        testPlaylist.addSong(mockSong);
        assertTrue(testPlaylist.getSongs().get(0).getTitle().equals("mock song"));
    }

    @Test
    public void testAddInvalidSong(){
        thrown.expect(IllegalArgumentException.class);
        testPlaylist.addSong(null);
    }

    @Test
    public void testRemoveValidSong(){
        mockSong = mock(Song.class);
        testPlaylist.addSong(mockSong);
        assertTrue(testPlaylist.getSongs().contains(mockSong));
        testPlaylist.removeSong(mockSong);
        assertTrue(!testPlaylist.getSongs().contains(mockSong));
    }
    @Test
    public void testRemoveInvalidSong(){
        mockSong = mock(Song.class);
        thrown.expect(IllegalArgumentException.class);
        testPlaylist.removeSong(null);
        thrown.expect(IllegalArgumentException.class);
        testPlaylist.removeSong(mockSong);
    }
}
