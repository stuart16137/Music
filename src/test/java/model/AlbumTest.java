
package model;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

public class AlbumTest {

    private Album testAlbum;
    private Artist mockArtist;
    private ArrayList<Song> mockSongs;
    private Song mockSong;

    @Rule
    public ExpectedException thrown= ExpectedException.none();

    @Before
    public void setUp(){
        mockArtist = mock(Artist.class);
        when(mockArtist.getName()).thenReturn("test artist");
        mockSong = mock(Song.class);
        when(mockSong.getTitle()).thenReturn("test song");
        mockSongs = mock(ArrayList.class);
        when(mockSongs.get(0)).thenReturn(mockSong);
        testAlbum = new Album("test", "20/02/1999", mockArtist, mockSongs);
    }

    @Test
    public void createValidAlbum() {
        testAlbum = new Album("test", "20/02/1999", mockArtist, mockSongs);
        assertNotNull(testAlbum.getTitle());
        assertNotNull(testAlbum.getArtist());
        assertNotNull(testAlbum.getReleaseDate());
        assertNotNull(testAlbum.getSongs());
    }

    @Test
    public void setValidTitle() {
        testAlbum.setTitle("new title");
        assertTrue(testAlbum.getTitle().equals("new title"));
    }

    @Test
    public void setInvalidTitle() {
        thrown.expect(IllegalArgumentException.class);
        testAlbum.setTitle(null);
        thrown.expect(IllegalArgumentException.class);
        testAlbum.setTitle("");
    }

    @Test
    public void setValidReleaseDate() {
        testAlbum.setReleaseDate("20/02/1998");
        assertTrue(testAlbum.getReleaseDate().equals("20/02/1998"));
    }

    @Test
    public void setInvalidReleaseDate() {
        thrown.expect(IllegalArgumentException.class);
        testAlbum.setReleaseDate("");
        thrown.expect(IllegalArgumentException.class);
        testAlbum.setReleaseDate("20-02/1999");
        thrown.expect(IllegalArgumentException.class);
        testAlbum.setReleaseDate("2/02/1999");
        thrown.expect(IllegalArgumentException.class);
        testAlbum.setReleaseDate("20/2/1999");
        thrown.expect(IllegalArgumentException.class);
        testAlbum.setReleaseDate("20/02/199");
    }

    @Test
    public void setValidArtist() {
        Artist newMock = mock(Artist.class);
        when(newMock.getName()).thenReturn("newMock");
        testAlbum.setArtist(newMock);
        assertTrue(testAlbum.getArtist().getName().equals("newMock"));
    }

    @Test
    public void setInvalidArtist() {
        Artist newMock = mock(Artist.class);
        thrown.expect(IllegalArgumentException.class);
        testAlbum.setArtist(newMock);
        thrown.expect(IllegalArgumentException.class);
        testAlbum.setArtist(null);
    }

    @Test
    public void setValidSongs() {
        ArrayList<Song> newMock = mock(ArrayList.class);
        Song mockS = mock(Song.class);
        when(newMock.get(0)).thenReturn(mockS);
        when(mockS.getTitle()).thenReturn("mock song");
        testAlbum.setSongs(newMock);
        assertTrue(testAlbum.getSongs().get(0).getTitle().equals("mock song"));
    }

    @Test
    public void setInvalidSongs() {
        ArrayList<Song> newMock = mock(ArrayList.class);
        thrown.expect(IllegalArgumentException.class);
        testAlbum.setSongs(newMock);
        thrown.expect(IllegalArgumentException.class);
        testAlbum.setArtist(null);
    }
}