package model;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class QueueTest {

    private Queue testQ;
    private Song mockSong;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp() throws Exception{
        testQ = new Queue();
        mockSong = mock(Song.class);
        when(mockSong.getTitle()).thenReturn("test song");
    }

    @Test
    public void createQueue(){
        testQ = new Queue();
        assertNotNull(testQ);
    }

    @Test
    public void addValidSongToQueue(){
        testQ.add(mockSong);
        assertTrue(testQ.contains(mockSong));
    }

    @Test
    public void addInvalidSongToQueue(){
        thrown.expect(IllegalArgumentException.class);
        testQ.add(null);
    }

    @Test
    public void containsTest(){
        Song mockedSong2 = mock(Song.class);
        testQ.add(mockSong);
        assertTrue(testQ.contains(mockSong));
        assertFalse(testQ.contains(mockedSong2));
        assertFalse(testQ.contains(null));
    }

    @Test
    public void removeValidSong(){
        testQ.add(mockSong);
        testQ.remove(mockSong);
        assertFalse(testQ.contains(mockSong));
    }

    @Test
    public void removeInvalidSong(){
        thrown.expect(IllegalArgumentException.class);
        testQ.remove(null);
        thrown.expect(IllegalArgumentException.class);
        testQ.remove(mockSong);
    }

    @Test
    public void testOrderOfQueue(){
        Song mockedSong2 = mock(Song.class);
        Song mockedSong3 = mock(Song.class);
        when(mockedSong2.getTitle()).thenReturn("test2");
        when(mockedSong3.getTitle()).thenReturn("test3");

        testQ.add(mockSong);
        testQ.add(mockedSong2);
        assertTrue(testQ.get(0).getTitle().equals("test song"));
        assertTrue(testQ.get(1).getTitle().equals("test2"));
        testQ.add(mockedSong3);
        assertTrue(testQ.get(0).getTitle().equals("test song"));
        assertTrue(testQ.get(1).getTitle().equals("test2"));
        assertTrue(testQ.get(2).getTitle().equals("test3"));

        assertTrue(testQ.removeHead().getTitle().equals("test song"));
        assertTrue(testQ.removeHead().getTitle().equals("test2"));
        assertTrue(testQ.removeHead().getTitle().equals("test3"));

        thrown.expect(IndexOutOfBoundsException.class);
        testQ.removeHead();
    }

    @Test
    public void getTest(){
        testQ.add(mockSong);
        assertTrue(testQ.get(0).getTitle().equals("test song"));

        thrown.expect(IndexOutOfBoundsException.class);
        testQ.get(1);
    }

    @Test
    public void moveTest(){
        Song mockedSong2 = mock(Song.class);
        Song mockedSong3 = mock(Song.class);
        when(mockedSong2.getTitle()).thenReturn("test2");
        when(mockedSong3.getTitle()).thenReturn("test3");

        testQ.add(mockSong);
        testQ.add(mockedSong2);
        testQ.add(mockedSong3);

        testQ.move(mockedSong3, 1);

        assertTrue(testQ.get(0).getTitle().equals("test song"));
        assertTrue(testQ.get(1).getTitle().equals("test3"));
        assertTrue(testQ.get(2).getTitle().equals("test2"));
    }
}
