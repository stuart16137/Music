package model;

import org.junit.Rule;
import org.junit.Test;
import org.junit.Before;
import org.junit.rules.ExpectedException;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class SongTest {

    private Song testSong;
    private Artist mockedArtist;
    private Album mockedAlbum;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp() throws Exception {
        mockedArtist = mock(Artist.class);
        mockedAlbum = mock(Album.class);
        when(mockedArtist.getName()).thenReturn("TestArtist");
        when(mockedAlbum.getTitle()).thenReturn("TestAlbum");

        testSong = new Song("Test", mockedArtist, mockedAlbum, 10.00);
    }

    @Test
    public void testCreateNonEmptySong(){
        testSong = new Song("Test", mockedArtist, mockedAlbum, 3.20);
        assertNotNull(testSong);
        assertNotNull(testSong.getArtist().getName());
        assertNotNull(testSong.getAlbum().getTitle());
        assertEquals(testSong.getRunTime(), 3.20);
    }

    @Test
    public void setValidTitle() {
        testSong.setTitle("NewTest");
        assertTrue(testSong.getTitle().equals("NewTest"));
    }

    @Test
    public void setValidArtist() {
        Artist newArtist = mock(Artist.class);
        when(newArtist.getName()).thenReturn("newArtist");

        testSong.setArtist(newArtist);
        assertTrue(testSong.getArtist().getName().equals("newArtist"));
    }

    @Test
    public void setInvalidArtist() {
        Artist newArtist = mock(Artist.class);
        thrown.expect(IllegalArgumentException.class);

        testSong.setArtist(newArtist);
    }

    @Test
    public void setValidAlbum() {
        Album newAlbum = mock(Album.class);
        when(newAlbum.getTitle()).thenReturn("newAlbum");

        testSong.setAlbum(newAlbum);
        assertTrue(testSong.getAlbum().getTitle().equals("newAlbum"));
    }

    @Test
    public void setInvalidAlbum() {
        Album newAlbum = mock(Album.class);
        thrown.expect(IllegalArgumentException.class);

        testSong.setAlbum(newAlbum);
    }

    @Test
    public void getTitle() {
        assertTrue(testSong.getTitle().equals("Test"));
    }

    @Test
    public void getAlbum() {
        assertTrue(testSong.getAlbum().getTitle().equals("TestAlbum"));
    }

    @Test
    public void getArtist() {
        assertTrue(testSong.getArtist().getName().equals("TestArtist"));
    }
}